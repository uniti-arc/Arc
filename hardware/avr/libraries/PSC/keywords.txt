#######################################
# Syntax Coloring Map PSC
#######################################

#######################################
# Datatypes (KEYWORD1)
#######################################

PSC		KEYWORD1

#######################################
# Methods and Functions (KEYWORD2)
#######################################
begin			KEYWORD2
start			KEYWORD2
stop			KEYWORD2
end				KEYWORD2
powerWrite		KEYWORD2
analogWrite		KEYWORD2
release			KEYWORD2
lock			KEYWORD2
unlock			KEYWORD2
getMode			KEYWORD2
attachInterrupt	KEYWORD2
detachInterrupt	KEYWORD2
getExtEvent0	KEYWORD2
getExtEvent1	KEYWORD2
getExtEvent2	KEYWORD2
getEndOfCycle	KEYWORD2

#######################################
# Constants (LITERAL1)
#######################################
PSC_OFF				LITERAL1
PSC_MOTOR			LITERAL1
PSC_PWM				LITERAL1
PSC_INPUT_IGNORE	LITERAL1
PSC_COMPARATOR		LITERAL1
PSC_EXTERNAL		LITERAL1
PSC_NUM_INTERRUPTS	LITERAL1
PSC_END_INT			LITERAL1
PSC_FAULT_INT		LITERAL1
