/*
 * Copyright (c) 2016 by Simon Wrafter <simon@teamuniti.com>
 * Power Stage Controller library for Uniti ARC.
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU General Public License version 2
 * or the GNU Lesser General Public License version 2.1, both as
 * published by the Free Software Foundation.
 */

#include "Arduino.h"
#include "PSC_C.h"
#include <avr/pgmspace.h>

const uint16_t sin_table[401] PROGMEM =
{
	0.00000, 0.00393, 0.00785, 0.01178, 0.01571, 0.01963, 0.02356, 0.02749,
	0.03141, 0.03534, 0.03926, 0.04318, 0.04711, 0.05103, 0.05495, 0.05887,
	0.06279, 0.06671, 0.07063, 0.07454, 0.07846, 0.08237, 0.08629, 0.09020,
	0.09411, 0.09802, 0.10192, 0.10583, 0.10973, 0.11364, 0.11754, 0.12144,
	0.12533, 0.12923, 0.13312, 0.13701, 0.14090, 0.14479, 0.14867, 0.15255,
	0.15643, 0.16031, 0.16419, 0.16806, 0.17193, 0.17580, 0.17966, 0.18352,
	0.18738, 0.19124, 0.19509, 0.19894, 0.20279, 0.20663, 0.21047, 0.21431,
	0.21814, 0.22197, 0.22580, 0.22963, 0.23345, 0.23726, 0.24108, 0.24488,
	0.24869, 0.25249, 0.25629, 0.26008, 0.26387, 0.26766, 0.27144, 0.27522,
	0.27899, 0.28276, 0.28652, 0.29028, 0.29404, 0.29779, 0.30154, 0.30528,
	0.30902, 0.31275, 0.31648, 0.32020, 0.32392, 0.32763, 0.33134, 0.33504,
	0.33874, 0.34243, 0.34612, 0.34980, 0.35347, 0.35715, 0.36081, 0.36447,
	0.36812, 0.37177, 0.37542, 0.37905, 0.38268, 0.38631, 0.38993, 0.39354,
	0.39715, 0.40075, 0.40434, 0.40793, 0.41151, 0.41509, 0.41866, 0.42222,
	0.42578, 0.42933, 0.43287, 0.43641, 0.43994, 0.44346, 0.44698, 0.45049,
	0.45399, 0.45749, 0.46097, 0.46446, 0.46793, 0.47140, 0.47486, 0.47831,
	0.48175, 0.48519, 0.48862, 0.49204, 0.49546, 0.49887, 0.50227, 0.50566,
	0.50904, 0.51242, 0.51579, 0.51915, 0.52250, 0.52584, 0.52918, 0.53251,
	0.53583, 0.53914, 0.54244, 0.54574, 0.54902, 0.55230, 0.55557, 0.55883,
	0.56208, 0.56533, 0.56856, 0.57179, 0.57501, 0.57821, 0.58141, 0.58460,
	0.58779, 0.59096, 0.59412, 0.59728, 0.60042, 0.60356, 0.60668, 0.60980,
	0.61291, 0.61601, 0.61909, 0.62217, 0.62524, 0.62830, 0.63135, 0.63439,
	0.63742, 0.64044, 0.64346, 0.64646, 0.64945, 0.65243, 0.65540, 0.65836,
	0.66131, 0.66425, 0.66718, 0.67010, 0.67301, 0.67591, 0.67880, 0.68168,
	0.68455, 0.68740, 0.69025, 0.69309, 0.69591, 0.69873, 0.70153, 0.70432,
	0.70711, 0.70988, 0.71264, 0.71539, 0.71813, 0.72085, 0.72357, 0.72627,
	0.72897, 0.73165, 0.73432, 0.73698, 0.73963, 0.74227, 0.74489, 0.74751,
	0.75011, 0.75270, 0.75528, 0.75785, 0.76041, 0.76295, 0.76548, 0.76800,
	0.77051, 0.77301, 0.77550, 0.77797, 0.78043, 0.78288, 0.78532, 0.78774,
	0.79016, 0.79256, 0.79494, 0.79732, 0.79968, 0.80204, 0.80438, 0.80670,
	0.80902, 0.81132, 0.81361, 0.81589, 0.81815, 0.82040, 0.82264, 0.82487,
	0.82708, 0.82928, 0.83147, 0.83364, 0.83581, 0.83796, 0.84009, 0.84222,
	0.84433, 0.84643, 0.84851, 0.85058, 0.85264, 0.85469, 0.85672, 0.85874,
	0.86074, 0.86273, 0.86471, 0.86668, 0.86863, 0.87057, 0.87250, 0.87441,
	0.87631, 0.87819, 0.88006, 0.88192, 0.88377, 0.88560, 0.88741, 0.88922,
	0.89101, 0.89278, 0.89454, 0.89629, 0.89803, 0.89975, 0.90146, 0.90315,
	0.90483, 0.90649, 0.90814, 0.90978, 0.91140, 0.91301, 0.91461, 0.91619,
	0.91775, 0.91931, 0.92085, 0.92237, 0.92388, 0.92538, 0.92686, 0.92832,
	0.92978, 0.93121, 0.93264, 0.93405, 0.93544, 0.93682, 0.93819, 0.93954,
	0.94088, 0.94220, 0.94351, 0.94481, 0.94609, 0.94735, 0.94860, 0.94984,
	0.95106, 0.95226, 0.95345, 0.95463, 0.95579, 0.95694, 0.95807, 0.95919,
	0.96029, 0.96138, 0.96246, 0.96351, 0.96456, 0.96559, 0.96660, 0.96760,
	0.96858, 0.96955, 0.97051, 0.97145, 0.97237, 0.97328, 0.97417, 0.97505,
	0.97592, 0.97677, 0.97760, 0.97842, 0.97922, 0.98001, 0.98079, 0.98154,
	0.98229, 0.98302, 0.98373, 0.98443, 0.98511, 0.98578, 0.98643, 0.98707,
	0.98769, 0.98830, 0.98889, 0.98946, 0.99002, 0.99057, 0.99110, 0.99161,
	0.99211, 0.99260, 0.99307, 0.99352, 0.99396, 0.99438, 0.99479, 0.99518,
	0.99556, 0.99592, 0.99627, 0.99660, 0.99692, 0.99722, 0.99750, 0.99777,
	0.99803, 0.99827, 0.99849, 0.99870, 0.99889, 0.99907, 0.99923, 0.99938,
	0.99951, 0.99962, 0.99972, 0.99981, 0.99988, 0.99993, 0.99997, 0.99999,
	1.00000
};


uint8_t _mode;
//float psc_fastSin(float angle);

void psc_begin(uint8_t mode, uint8_t input) {
	//Start PLL
	PLLCSR = _BV(PLLF) | _BV(PLLE);
	while (!(PLLCSR & _BV(PLOCK))) {}

	if (mode == PSC_MOTOR) {
		_mode = PSC_MOTOR;

		PMIC0 = _BV(PFLTE0);
		PMIC1 = _BV(PFLTE1);
		PMIC2 = _BV(PFLTE2);

		PCTL = _BV(PPRE0) | _BV(PCLKSEL); //PLL, div by 4 => 16 MHz clock
		POCR_RB = 0x640; //1600

		POC = 0x3F; //enable all outputs
	} else if(mode == PSC_PWM) {
		_mode = PSC_PWM;

		PMIC0 = _BV(POVEN0) | _BV(PELEV0) | _BV(PFLTE0);
		PMIC1 = _BV(POVEN1) | _BV(PELEV1) | _BV(PFLTE1);
		PMIC2 = _BV(POVEN2) | _BV(PELEV2) | _BV(PFLTE2);

		PCTL = _BV(PPRE1) | _BV(PPRE0) | _BV(PCLKSEL) | _BV(PRUN); //PLL, div by 256 => 250 kHz clock
		POCR_RB = 0x00FF; //255
	}

	PCNF = 0x00;

	if (input == PSC_COMPARATOR) {
		PMIC0 |= _BV(PISEL0) | _BV(PRFM02);
		PMIC1 |= _BV(PISEL1) | _BV(PRFM12);
		PMIC2 |= _BV(PISEL2) | _BV(PRFM22);
	} else if (input == PSC_EXTERNAL) {
		PMIC0 |= _BV(PRFM02);
		PMIC1 |= _BV(PRFM12);
		PMIC2 |= _BV(PRFM22);
	} // else if (input == PSC_INPUT_IGNORE)
}

void psc_start(void) {
	PCTL |= _BV(PRUN);
}

void psc_stop(void) {
	PCTL &= ~_BV(PRUN);
}
void psc_end(void) {
	//Stop PSC
	PMIC0 = 0x00;
	PMIC1 = 0x00;
	PMIC2 = 0x00;
	POC = 0x00;
	PCTL = 0x00;
	PIM = 0x00;
	//Stop PLL
	PLLCSR = 0x00;
	_mode = PSC_OFF;
}

void psc_powerWrite(uint16_t angle, float power) {
	if (_mode == PSC_MOTOR) {
		uint16_t angle_1 = angle;
		uint16_t angle_2 = (angle + 533) % 1600; //add 2/3*pi
		uint16_t angle_3 = (angle + 1067) % 1600; //add 4/3*pi
		uint16_t phase_1 = 0;
		uint16_t phase_2 = 0;
		uint16_t phase_3 = 0;

		if (angle_1 <= 400) {
			phase_1 = sin_table[angle_1] * power * 1600.0;
		} else if (angle_1 <= 800) {
			phase_1 = sin_table[800 - angle_1] * power * 1600.0;
		} else if (angle_1 <= 1200) {
			phase_1 = -sin_table[angle_1-800] * power * 1600.0;
		} else if (angle_1 <= 1600) {
			phase_1 = -sin_table[1600 - angle_1] * power * 1600.0;
		}

		if (angle_2 <= 400) {
			phase_2 = sin_table[angle_2] * power * 1600.0;
		} else if (angle_2 <= 800) {
			phase_2 = sin_table[800 - angle_2] * power * 1600.0;
		} else if (angle_2 <= 1200) {
			phase_2 = -sin_table[angle_2-800] * power * 1600.0;
		} else if (angle_2 <= 1600) {
			phase_2 = -sin_table[1600 - angle_2] * power * 1600.0;
		}

		if (angle_3 <= 400) {
			phase_3 = sin_table[angle_3] * power * 1600.0;
		} else if (angle_3 <= 800) {
			phase_3 = sin_table[800 - angle_3] * power * 1600.0;
		} else if (angle_3 <= 1200) {
			phase_3 = -sin_table[angle_3-800] * power * 1600.0;
		} else if (angle_3 <= 1600) {
			phase_3 = -sin_table[1600 - angle_3] * power * 1600.0;
		}

		if (phase_1 >= 0) {
			POCR0SA = 0;
			POCR0RA = phase_1;
			POCR0SB = 1600;
		} else {
			POCR0SA = 0;
			POCR0RA = 0;
			POCR0SB = 1600 + phase_1;
		}
		if (phase_2 >= 0) {
			POCR1SA = 0;
			POCR1RA = phase_2;
			POCR1SB = 1600;
		} else {
			POCR1SA = 0;
			POCR1RA = 0;
			POCR1SB = 1600 + phase_2;
		}
		if (phase_3 >= 0) {
			POCR2SA = 0;
			POCR2RA = phase_3;
			POCR2SB = 1600;
		} else {
			POCR2SA = 0;
			POCR2RA = 0;
			POCR2SB = 1600 + phase_3;
		}
	}
}

void psc_analogWrite(uint8_t pin, uint8_t val) {
	if (_mode == PSC_PWM) {
		switch (pin) {
			case 10: //PSCOUT0A
				POC |= _BV(POEN0A);
				POCR0SA = 0;
				POCR0RA = val & 0xFF;
				break;
			case 13: //PSCOUT0B
				POC |= _BV(POEN0B);
				POCR0SB = 255-(val & 0xFF);
				break;
			case 3: //PSCOUT1A
				POC |= _BV(POEN1A);
				POCR1SA = 0;
				POCR1RA = val & 0xFF;
				break;
			case 5: //PSCOUT1B
				POC |= _BV(POEN1B);
				POCR1SB = 255-(val & 0xFF);
				break;
			case 12: //PSCOUT2A
				POC |= _BV(POEN2A);
				POCR2SA = 0;
				POCR2RA = val & 0xFF;
				break;
			case 11: //PSCOUT2B
				POC |= _BV(POEN2B);
				POCR2SB = 255-(val & 0xFF);
				break;
		}
	}
}

void psc_release(uint8_t pin) {
	if (_mode == PSC_PWM) {
		switch (pin) {
			case 10: //PSCOUT0A
				POC &= ~_BV(POEN0A);
				POCR0SA = 0;
				POCR0RA = 0;
				break;
			case 13: //PSCOUT0B
				POC &= ~_BV(POEN0B);
				POCR0SB = 255;
				break;
			case 3: //PSCOUT1A
				POC &= ~_BV(POEN1A);
				POCR1SA = 0;
				POCR1RA = 0;
				break;
			case 5: //PSCOUT1B
				POC &= ~_BV(POEN1B);
				POCR1SB = 255;
				break;
			case 12: //PSCOUT2A
				POC &= ~_BV(POEN2A);
				POCR2SA = 0;
				POCR2RA = 0;
				break;
			case 11: //PSCOUT2B
				POC &= ~_BV(POEN2B);
				POCR2SB = 255;
				break;
		}
	}
}

void psc_lock() {
	PCNF |= _BV(PULOCK);
}

void psc_unlock() {
	PCNF &= ~_BV(PULOCK);
}

uint8_t psc_getMode() {
	return _mode;
}

/*
// Fast sin(2*pi*x) approximation.
// The caller must make sure that the argument lies within [0..1).
// Courtesy of:
// https://namoseley.wordpress.com/2012/09/18/a-quick-and-dirty-sinx-approximation/
float psc_fastSin(float x){
	if (x < 0.5 ) {
		return -16.0*x*x + 8.0*x;
	}
	else {
		return 16.0*x*x - 24.0*x + 8.0;
	}
}
*/

boolean psc_getExtEvent0() {
	boolean status = PIFR & _BV(PEV0) ? true : false;
	PIFR |= _BV(PEV0); //clear bit
	return status;
}

boolean psc_getExtEvent1() {
	boolean status = PIFR & _BV(PEV1) ? true : false;
	PIFR |= _BV(PEV1); //clear bit
	return status;
}

boolean psc_getExtEvent2() {
	boolean status = PIFR & _BV(PEV2) ? true : false;
	PIFR |= _BV(PEV2); //clear bit
	return status;
}

boolean psc_getEndOfCycle() {
	boolean status = PIFR & _BV(PEOP) ? true : false;
	PIFR |= _BV(PEOP); //clear bit
	return status;
}


///////////////////////////////

typedef void (*voidFuncPtr)(void);

static void nothing(void) {}

voidFuncPtr funcVector[PSC_NUM_INTERRUPTS] = {
	nothing,
	nothing
};

void psc_attachInterrupt(uint8_t interruptNum, void (*userFunc)(void))
{
	if (interruptNum < PSC_NUM_INTERRUPTS) {
		if (interruptNum == PSC_END_INT) {
			PIM |= _BV(PEOPE);
		} else if (interruptNum == PSC_FAULT_INT) {
			PIM |= _BV(PEVE2) | _BV(PEVE1) | _BV(PEVE0);
		}
		funcVector[interruptNum] = userFunc;
	}
}


//TODO: turn interrupts off
void psc_detachInterrupt(uint8_t interruptNum) {
	if(interruptNum < PSC_NUM_INTERRUPTS) {
		funcVector[interruptNum] = nothing;
	}
}

ISR(PSC_FAULT_vect) {
	funcVector[PSC_FAULT_INT]();
}

ISR(PSC_EC_vect) {
	funcVector[PSC_END_INT]();
}
