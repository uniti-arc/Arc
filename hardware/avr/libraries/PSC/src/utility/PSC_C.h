/*
 * Copyright (c) 2016 by Simon Wrafter <simon@teamuniti.com>
 * Power Stage Controller library for Uniti ARC.
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU General Public License version 2
 * or the GNU Lesser General Public License version 2.1, both as
 * published by the Free Software Foundation.
 */

#ifndef _PSC_C_H_INCLUDED
#define _PSC_C_H_INCLUDED

//modes
#define PSC_OFF   0
#define PSC_MOTOR 1
#define PSC_PWM   2

//inputs
#define PSC_INPUT_IGNORE 0
#define PSC_COMPARATOR 1
#define PSC_EXTERNAL 2

//interrupts
#define PSC_NUM_INTERRUPTS 2
#define PSC_END_INT 0
#define PSC_FAULT_INT 1

void psc_begin(uint8_t mode, uint8_t input);
void psc_start();
void psc_stop();
void psc_end();

void psc_powerWrite(uint16_t angle, float power);
void psc_analogWrite(uint8_t pin, uint8_t val); //PWM mode
void psc_release(uint8_t pin);

void psc_lock();
void psc_unlock();

uint8_t psc_getMode();

boolean psc_getExtEvent0();
boolean psc_getExtEvent1();
boolean psc_getExtEvent2();
boolean psc_getEndOfCycle();

//Interrupts
void psc_attachInterrupt(uint8_t interruptNum, void (*userFunc)(void));
void psc_detachInterrupt(uint8_t interruptNum);

#endif
