/*
 * Copyright (c) 2016 by Simon Wrafter <simon@teamuniti.com>
 * Power Stage Controller library for Uniti ARC.
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU General Public License version 2
 * or the GNU Lesser General Public License version 2.1, both as
 * published by the Free Software Foundation.
 */

#include "Arduino.h"

extern "C" {
	#include "utility/PSC_C.h"
}

#include "PSC.h"

PSC_Class PSC;

void PSC_Class::begin(uint8_t mode) {
	psc_begin(mode, PSC_INPUT_IGNORE);
}

void PSC_Class::begin(uint8_t mode, uint8_t input) {
	psc_begin(mode, input);
}

void PSC_Class::start() {
	psc_start();
}

void PSC_Class::stop() {
	psc_stop();
}
void PSC_Class::end() {
	psc_end();
}

void PSC_Class::powerWrite(uint16_t angle, float power) {
	psc_powerWrite(angle, power);
}

void PSC_Class::analogWrite(uint8_t pin, uint8_t val) {
	psc_analogWrite(pin, val);
}

void PSC_Class::release(uint8_t pin) {
	psc_release(pin);
}

void PSC_Class::lock() {
	psc_lock();
}

void PSC_Class::unlock() {
	psc_unlock();
}

uint8_t PSC_Class::getMode() {
	return psc_getMode();
}

boolean PSC_Class::getExtEvent0() {
	return psc_getExtEvent0();
}
boolean PSC_Class::getExtEvent1() {
	return psc_getExtEvent1();
}
boolean PSC_Class::getExtEvent2() {
	return psc_getExtEvent2();
}
boolean PSC_Class::getEndOfCycle() {
	return psc_getEndOfCycle();
}

void PSC_Class::attachInterrupt(uint8_t interruptNum, void (*userFunc)(void)) {
	psc_attachInterrupt(interruptNum, userFunc);
}

void PSC_Class::detachInterrupt(uint8_t interruptNum) {
	psc_detachInterrupt(interruptNum);
}
