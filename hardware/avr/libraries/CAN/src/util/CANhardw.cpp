/*
 * CANhardw.cpp
 *
 * This file is part of the Uniti ARC CAN bus library.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: David Bengtsson
 */

#include <Arduino.h>
#include "CANhardw.h"
#include "RxBuffer.h"

const uint8_t NBR_OF_MOBS = 6;          /* Number of hardware specific MObs */
static uint32_t timer = 0;              /* Timer counter */

voidFuncPtr rx_isr_ptr = NULL;          /* Interrupt RX callback function */
voidFuncPtr tx_isr_ptr = NULL;          /* Interrupt TX callback function */

static void handle_rx(void);            /* ISR RX help function */
static void handle_tx(void);            /* ISR TX help function */
static void retr_data(CANframe &);      /* ISR retrieving data help function */

/************************************************************************/
/* init - Initialize CAN bus communication.                             */
/*                                                                      */
/* @param baud_rate: Desired CAN baud rate to use.                      */
/************************************************************************/
void CANhardw::init(uint8_t baud_rate, uint8_t mode)
{
        CANGCON = (1 << SWRES);         /* Reset the CAN controller */

        for (uint8_t mob = 0; mob < NBR_OF_MOBS; mob++) {
                CANPAGE = (mob << MOBNB0);      /* Select MOb */
                CANIDM = 0x00;          /* Initialize ID Mask Registers */
                CANSTMOB = 0x00;        /* Initialize MOb Status Register */
                CANCDMOB = 0x00;        /* Initialize MOb Control Register */
        }

        if (baud_rate == CAN_BAUD_1000K)        /*Set desired baud rate */
                CANBT1 = 0x02;
        else if (baud_rate == CAN_BAUD_500K)
                CANBT1 = 0x06;
        else if (baud_rate == CAN_BAUD_250K)
                CANBT1 = 0x0E;
        else if (baud_rate == CAN_BAUD_200K)
                CANBT1 = 0x12;
        else if (baud_rate == CAN_BAUD_125K)
                CANBT1 = 0x1E;
        else if (baud_rate == CAN_BAUD_100K)
                CANBT1 = 0x26;
        CANBT2 = 0x04;
        CANBT3 = 0x13;

        CANGIE = (1 << ENIT) |          /* Enable interrupts */
                 (1 << ENRX) |          /* Receive interrupt */
                 (1 << ENTX) |          /* Transmit interrupt */
                 (1 << ENOVRT);         /* Timer overflow interrupt */

        CANIE2 = 0x3F;          /* Enable interrupts for all MObs */
        CANIE1 = 0x00;          /* For future compatibility */

        if (mode == CAN_MODE_LISTEN)    /* Set CAN operating mode */
                CANGCON = (1 << LISTEN);
        else if (mode == CAN_MODE_TTC)
                CANGCON = (1 << TTC);

        if (mode != CAN_MODE_STNDBY)
                CANGCON |= (1 << ENASTB);
}

/************************************************************************/
/* setupRx - Setup RX MOb for receiving data.                           */
/*                                                                      */
/* @param mob: Desired MOb number.                                      */
/* @param id: Message ID to receive.                                    */
/* @param mask: Message ID mask.                                        */
/************************************************************************/
void CANhardw::setupRx(uint8_t mob, uint32_t id, uint32_t mask,
        CAN_FORMAT format)
{
        CANPAGE = (mob << MOBNB0);      /* Select MOb */

        if (format == CAN_FORMAT_EXT) { /* Set 29-bit ID and mask */
                CANIDT = (id << 3);
                CANIDM = (mask << 3);
                CANCDMOB = (1 << IDE);
        } else {                        /* Set 11-bit ID and mask */
                CANIDT = (id << 21);
                CANIDM = (mask << 21);
        }
        CANIDM |= (1 << IDEMSK);        /* Match ID format set in CANCDMOB */
        CANCDMOB |= (1 << CONMOB1);     /* Enable reception of data. */
}

/************************************************************************/
/* setupTx - Setup TX MOb for transmitting data.                        */
/*                                                                      */
/* @param nbr_of_rx_mobs: Number of RX MObs (for knowing where to split */
/*                        the total number of MObs).                    */
/* @param tx_frame: Desired frame to transmit.                          */
/*                                                                      */
/* @return CAN_BUSY if all TX MObs are busy, CAN_OK otherwise.          */
/************************************************************************/
CAN_RES CANhardw::setupTx(uint8_t nbr_of_rx_mobs, CANframe &tx_frame)
{
        uint8_t mob;
        for (mob = nbr_of_rx_mobs; mob < NBR_OF_MOBS; mob++) {
                if (!(CANEN2 & (1 << mob)))
                        break;
        }
        if (mob == NBR_OF_MOBS)         /* Check MOb availability */
                return CAN_BUSY;

        CANPAGE = (mob << MOBNB0);      /* Select MOb */

        if (tx_frame.isExtended()) {    /* Set 29-bit ID */
                CANIDT = (tx_frame.getId() << 3);
                CANCDMOB = (1 << IDE);
        } else {                        /* Set 11-bit ID */
                CANIDT = (tx_frame.getId() << 21);
        }

        if (tx_frame.isRequest()) {     /* Set request tag */
                CANIDT |= (1 << RTRTAG);
        } else {                        /* .. or write data to be sent */
                uint8_t *data = tx_frame.getData();
                for (uint8_t i = 0; i < tx_frame.getLength(); i++)
                        CANMSG = data[i];
        }
        CANCDMOB |= (1 << CONMOB0) |    /* Enable transmission of data */
                    tx_frame.getLength();       /* Set message data length */
        return CAN_OK;
}

/************************************************************************/
/* getErrors - Returns the current error count of either CAN line.      */
/*                                                                      */
/* @param line: Desired CAN line. Valid values are CAN_LINE_RX and      */
/*              CAN_LINE_TX.                                            */
/************************************************************************/
uint8_t CANhardw::getErrors(CAN_LINE line)
{
        return (line == CAN_LINE_RX) ? CANREC : CANTEC;
}

/************************************************************************/
/* Interrupt Service Routine for CAN interrupts.                        */
/************************************************************************/
ISR(CAN_INT_vect)
{
        uint8_t save_page = CANPAGE;    /* Save pre-interrupt CAN Page */

        while ((CANPAGE = CANHPMOB) < 0xF0) {   /* Select MOb CAN Page */
                uint8_t isr_flags = CANSTMOB;   /* Save interrupt flags */
                CANSTMOB = 0x00;        /* Clear MOb Status Register */

                if (isr_flags & (1 << RXOK))
                        handle_rx();    /* Receiving data interrupt */
                else if (isr_flags & (1 << TXOK))
                        handle_tx();    /* Transmitting data interrupt */
        }
        CANPAGE = save_page;    /* Restore pre-interrupt CAN Page */
}

/************************************************************************/
/* Interrupt Service Routine for CAN timer overflow.                    */
/************************************************************************/
ISR(CAN_TOVF_vect)
{
        timer += UINT16_MAX;
}

/************************************************************************/
/* handle_rx - Handle RXOK interrupt (help function).                   */
/************************************************************************/
void handle_rx(void)
{
        CANframe temp_frame;
        retr_data(temp_frame);

        if (rx_isr_ptr == NULL)
                RxBuffer::setData(temp_frame);  /* Store data */
        else
                rx_isr_ptr(temp_frame); /* .. or call callback function */

        if (temp_frame.isExtended())
                CANCDMOB = (1 << IDE);  /* Re-setup 29-bit reception */

        CANCDMOB |= (1 << CONMOB1);     /* Re-enable reception of data */
}

/************************************************************************/
/* handle_tx - Handle TXOK interrupt (help function).                   */
/************************************************************************/
void handle_tx(void)
{
        if (tx_isr_ptr == NULL) {
                CANCDMOB = 0x00;        /* Clear MOb Control Register */
        } else {
                CANframe temp_frame;
                retr_data(temp_frame);
                tx_isr_ptr(temp_frame); /* Call callback function */
        }
}

/************************************************************************/
/* retr_data - Retrieve new CAN data (help function).                   */
/*                                                                      */
/* @param frame: Frame to transfer data to.                             */
/************************************************************************/
void retr_data(CANframe &frame)
{
        if (CANCDMOB & (1 << IDE)) {    /* Check message ID and frame type */
                frame.setId(CANIDT >> 3);
        } else {
                frame.setFormat(CAN_FORMAT_STD);
                frame.setId(CANIDT >> 21);
        }
        uint8_t dlc = CANCDMOB & 0x0F;  /* Read received data length */
        CANCDMOB = 0x00;                /* Clear MOb Control Register */

        if (CANIDT & (1 << RTRTAG)) {   /* Set request tag */
                frame.setFrameType(CAN_FRAME_REQ);
                frame.setLength(dlc);
        } else {                        /* .. or read retrieved data */
                uint8_t data[dlc];
                for (uint8_t byte = 0; byte < dlc; byte++)
                        data[byte] = CANMSG;
                frame.setData(dlc, data);
        }
        frame.setTimeStamp(timer + CANSTM);
}