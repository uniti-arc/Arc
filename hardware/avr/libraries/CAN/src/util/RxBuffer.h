/*
 * RxBuffer.h
 *
 * This file is part of the Uniti ARC CAN bus library.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: David Bengtsson
 */

#ifndef RXBUFFER_H
#define RXBUFFER_H

#include "../CANframe.h"

/************************************************************************/
/* Global function declarations.                                        */
/************************************************************************/
namespace RxBuffer
{
        uint8_t available(void);
        CAN_RES getData(CANframe &);
        uint32_t getId(void);
        void setData(CANframe &);
}

#endif