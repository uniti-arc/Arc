/*
 * RxBuffer.cpp
 *
 * This file is part of the Uniti ARC CAN bus library.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: David Bengtsson
 */

#include <util/atomic.h>
#include "RxBuffer.h"

static const uint8_t BUFFER_ELEMENTS = 8;       /*  Buffer size */
static const uint8_t BUFFER_SIZE = BUFFER_ELEMENTS + 1; /* Memory-wise size */

static CANframe buffer[BUFFER_SIZE];    /* Buffer stored as an array */
static uint8_t first = 0;               /* First object in buffer */
static uint8_t last = 0;                /* Last object in buffer */

/************************************************************************/
/* available - Returns whether there are data in the buffer or not.     */
/*                                                                      */
/* @return The number of messages available.                            */
/************************************************************************/
uint8_t RxBuffer::available(void)
{
        int8_t res;
        ATOMIC_BLOCK(ATOMIC_FORCEON) {
                res = (last - first) % BUFFER_SIZE;
        }
        if (res < 0)
                res += BUFFER_SIZE;
        return res;
}

/************************************************************************/
/* getData - Get data from buffer.                                      */
/*                                                                      */
/* @param frame: CAN frame to update.                                   */
/*                                                                      */
/* @return CAN_OK if there is data in the buffer, CAN_NO_MSG otherwise. */
/************************************************************************/
CAN_RES RxBuffer::getData(CANframe &frame)
{
        if (!RxBuffer::available())
                return CAN_NO_MSG;

        ATOMIC_BLOCK(ATOMIC_FORCEON) {
                frame = buffer[first];
        }
        first = (first + 1) % BUFFER_SIZE;
        return CAN_OK;
}

/************************************************************************/
/* getId - Get the ID of the first message in the buffer.               */
/*                                                                      */
/* @return The ID if data is available, CAN_NO_ID otherwise.            */
/************************************************************************/
uint32_t RxBuffer::getId(void)
{
        if (RxBuffer::available()) {
                uint32_t id;
                ATOMIC_BLOCK(ATOMIC_FORCEON) {
                        id = buffer[first].getId();
                }
                return id;
        }
        return CAN_NO_ID;
}

/************************************************************************/
/* setData - Write data to buffer.                                      */
/*                                                                      */
/* @param frame: CAN frame to store.                                    */
/************************************************************************/
void RxBuffer::setData(CANframe &frame)
{
        buffer[last] = frame;

        last = (last + 1) % BUFFER_SIZE;
        if (last == first) first = (first + 1) % BUFFER_SIZE;
}