/*
 * CAN.cpp
 *
 * This file is part of the Uniti ARC CAN bus library.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: David Bengtsson
 */

#include "CAN.h"
#include "util/CANhardw.h"
#include "util/RxBuffer.h"

const uint8_t CAN_BFFR_FULL = 8;        /* CAN buffer full constant */
CANb CAN;                               /* CAN bus object */

/************************************************************************/
/* CANb - Class constructor.                                            */
/************************************************************************/
CANb::CANb(void) : rx_mob_cnt(0) {}

/************************************************************************/
/* begin - Initialization of CAN bus communication.                     */
/*                                                                      */
/* The bus is initialized with default baud rate (125 kBd) and default  */
/* number of RX MObs (3).                                               */
/*                                                                      */
/* @return CAN_OK when the bus is initialized.                          */
/************************************************************************/
CAN_RES CANb::begin(void)
{
        return CANb::begin(CAN_BAUD_125K, (NBR_OF_MOBS / 2), CAN_MODE_ENABLE);
}

/************************************************************************/
/* begin - Initialization of CAN bus communication.                     */
/*                                                                      */
/* The bus is initialized with default number of RX MObs (3).           */
/*                                                                      */
/* @param baud_rate: Desired CAN baud rate to use.                      */
/*                                                                      */
/* @return CAN_OK when the bus is initialized.                          */
/************************************************************************/
CAN_RES CANb::begin(CAN_BAUD baud_rate)
{
        return CANb::begin(baud_rate, (NBR_OF_MOBS / 2), CAN_MODE_ENABLE);
}

/************************************************************************/
/* begin - Initialization of CAN bus communication.                     */
/*                                                                      */
/* @param baud_rate: Desired CAN baud rate to use.                      */
/* @param nbr_of_rx_mobs: Number of RX MObs to use.                     */
/*                                                                      */
/* @return CAN_VAL_OOR if desired number of RX MObs is out of range,    */
/*         CAN_OK when the bus is initialized.                          */
/************************************************************************/
CAN_RES CANb::begin(CAN_BAUD baud_rate, uint8_t nbr_of_rx_mobs)
{
        return CANb::begin(baud_rate, nbr_of_rx_mobs, CAN_MODE_ENABLE);
}

/************************************************************************/
/* begin - Initialization of CAN bus communication.                     */
/*                                                                      */
/* @param baud_rate: Desired CAN baud rate to use.                      */
/* @param nbr_of_rx_mobs: Number of RX MObs to use.                     */
/* @param mode: CAN mode to use. Valid values are CAN_MODE_ENABLE,      */
/*              CAN_MODE_LISTEN, CAN_MODE_TTC and CAN_MODE_STNDBY.      */
/*                                                                      */
/* @return CAN_VAL_OOR if desired number of RX MObs is out of range,    */
/*         CAN_OK when the bus is initialized.                          */
/************************************************************************/
CAN_RES CANb::begin(CAN_BAUD baud_rate, uint8_t nbr_of_rx_mobs, CAN_MODE mode)
{
        if (nbr_of_rx_mobs <= NBR_OF_MOBS)      /* Check number of RX MObs */
                this->nbr_of_rx_mobs = nbr_of_rx_mobs;
        else
                return CAN_VAL_OOR;

        CANhardw::init(baud_rate, mode);
        return CAN_OK;
}

/************************************************************************/
/* attachInterrupt - Enable CAN interrupt callback.                     */
/*                                                                      */
/* @param line: Desired CAN line to enable callback for. Valid values   */
/*              are CAN_LINE_RX and CAN_LINE_TX.                        */
/* @param func: Callback function that returns void and takes a single  */
/*              CANframe object reference(!) as parameter.              */
/************************************************************************/
void CANb::attachInterrupt(CAN_LINE line, voidFuncPtr func)
{
        switch (line) {
        case CAN_LINE_RX :
                rx_isr_ptr = func;
                break;
        case CAN_LINE_TX :
                tx_isr_ptr = func;
                break;
        }
}

/************************************************************************/
/* setRxMask - Setup Rx MOb for receiving data.                         */
/*                                                                      */
/* @param id: Message base ID to receive (29-bit ID supported).         */
/* @param mask: ID mask to apply.                                       */
/*                                                                      */
/* @return CAN_VAL_OOR if desired ID is Out Of Range,                   */
/*         CAN_OK if the mask is set up successfully.                   */
/************************************************************************/
CAN_RES CANb::setRxMask(uint32_t id, uint32_t mask)
{
        return CANb::setRxMask(id, mask, CAN_FORMAT_EXT);
}

/************************************************************************/
/* setRxMask - Setup Rx MOb for receiving data.                         */
/*                                                                      */
/* @param id: Message base ID to receive (ID length depends on format). */
/* @param mask: ID mask to apply.                                       */
/* @param format: Extended frame or not? Supported frame types are      */
/*                CAN_STD_FR (11-bit ID) vs CAN_EXT_FR (29-bit ID).     */
/*                                                                      */
/* @return CAN_VAL_OOR if desired ID is Out Of Range,                   */
/*         CAN_LIM_RCHD for reaching the limit of RX MObs,              */
/*         CAN_OK if the mask is set up successfully.                   */
/************************************************************************/
CAN_RES CANb::setRxMask(uint32_t id, uint32_t mask, CAN_FORMAT format)
{
        if (rx_mob_cnt == nbr_of_rx_mobs)       /* Check RX MOb quantity */
                return CAN_LIM_RCHD;

        if (format == CAN_FORMAT_EXT) {         /* Check ID length */
                if (id > MAX_ID_EXT)
                        return CAN_VAL_OOR;
        } else {
                if (id > MAX_ID_STD)
                        return CAN_VAL_OOR;
        }
        CANhardw::setupRx(rx_mob_cnt++, id, mask, format);
        return CAN_OK;
}

/************************************************************************/
/* available - Returns whether there are data available or not.         */
/*                                                                      */
/* Note: The constant CAN_BFFR_FULL can be used to determine the status */
/* of the buffer. If the buffer appears to be full, one should highly   */
/* consider the way one uses the bus or anything that affects it.       */
/*                                                                      */
/* @return The number of messages available.                            */
/************************************************************************/
uint8_t CANb::available(void)
{
        return RxBuffer::available();
}

/************************************************************************/
/* getErrorCnt - Returns the current error count of either CAN line.    */
/*                                                                      */
/* @param line: Desired CAN line. Valid values are CAN_LINE_RX and      */
/*              CAN_LINE_TX.                                            */
/*                                                                      */
/* @return According to title.                                          */
/************************************************************************/
uint8_t CANb::getErrorCnt(CAN_LINE line)
{
        return CANhardw::getErrors(line);
}

/************************************************************************/
/* getId - Returns the ID of the first message in the buffer.           */
/*                                                                      */
/* @return The ID if data is available, CAN_NO_ID otherwise.            */
/************************************************************************/
uint32_t CANb::getId(void)
{
        return RxBuffer::getId();
}

/************************************************************************/
/* read - Read data from buffer if available.                           */
/*                                                                      */
/* @param frame: Frame to transfer data to.                             */
/*                                                                      */
/* @return CAN_OK if read operation succeeded, CAN_NO_MSG otherwise.    */
/************************************************************************/
CAN_RES CANb::read(CANframe &frame)
{
        return RxBuffer::getData(frame);
}

/************************************************************************/
/* write - Transmit data on the CAN bus.                                */
/*                                                                      */
/* @param tx_frame: Desired frame to transmit.                          */
/*                                                                      */
/* @return CAN_OK if the transmit setup succeeded,                      */
/*         CAN_BUSY if all TX MObs are busy.                            */
/************************************************************************/
CAN_RES CANb::write(CANframe &tx_frame)
{
        return CANhardw::setupTx(nbr_of_rx_mobs, tx_frame);
}