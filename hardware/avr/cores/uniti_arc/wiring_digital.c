/*
  wiring_digital.c - digital input and output functions
  Part of Arduino - http://www.arduino.cc/

  Copyright (c) 2005-2006 David A. Mellis


  Modified to support ATmega32M1, ATmega64M1, etc.   Mar 2016
        Al Thomason:   https://github.com/thomasonw/ATmegaxxM1-C1
                       http://smartmppt.blogspot.com/search/label/xxM1-IDE




  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place, Suite 330,
  Boston, MA  02111-1307  USA

  Modified 28 September 2010 by Mark Sproul
*/

#define ARDUINO_MAIN
#include "wiring_private.h"
#include "pins_arduino.h"

void pinMode(uint8_t pin, uint8_t mode)
{
	uint8_t bit = digitalPinToBitMask(pin);
	uint8_t port = digitalPinToPort(pin);
	volatile uint8_t *reg, *out;


#if defined(__AVR_ATmega32M1__) || defined(__AVR_ATmega64M1__)

	// AT: Setting up an Differential channel?
	// AT: Using very crude and simple re-entrant calling of pinMode() to pre-configure physical ports...
	if (pin == AD0){
		pinMode (4, INPUT);						// AMP0- is D4
		pinMode (15, INPUT);					// AMP0+ is D15

		sbi(DIDR1, AMP0ND);						// AMP0-, disable digital input
		sbi(DIDR1, AMP0PD);						// AMP0+, disable digital input

		AMP0CSR &= _BV(AMPCMP0);				// clear all but Comparator connection bit
		AMP0CSR |= ((mode & 0x03) << AMP0G0);	// gain = passed 'mode'
		sbi(AMP0CSR, AMP0EN);					// enable AMP0

	} else if (pin == AD1){
		pinMode (A4, INPUT);					// AMP1- is A4
		pinMode (A5, INPUT);					// AMP1+ is A5

		sbi(DIDR1, ADC8D);						// AMP1-, disable digital input
		sbi(DIDR1, ADC9D);						// AMP1+, disable digital input

		AMP1CSR &= _BV(AMPCMP1);				// clear all but Comparator connection bit
		AMP1CSR = ((mode & 0x03) << AMP1G0);	// gain = passed 'mode'
		sbi(AMP1CSR, AMP1EN);					// enable AMP1

    } else if (pin == AD2){
		pinMode (2, INPUT);						// AMP2- is D2
		pinMode (14, INPUT);					// AMP2+ is D14

		sbi(DIDR0, ADC6D);						// AMP2-, disable digital input
		sbi(DIDR1, AMP2PD);						// AMP2+, disable digital input

		AMP2CSR &= _BV(AMPCMP2);				// clear all but Comparator connection bit
		AMP2CSR = ((mode & 0x03) << AMP2G0);	// gain = passed 'mode'
		sbi(AMP2CSR, AMP2EN);					// enable AMP2

	// Analog Comparators, start up sequence
	} else if(pin == AC0) {
		if (mode < 7) {
			pinMode(7, INPUT);					//ACMP0 is D7
			sbi(DIDR1, ACMP0D);					//D7, diasable digital input
		} else {
			AMP0CSR |= _BV(AMPCMP0);			//connect output from analog amplifier 0
		}
		mode &= 0x07;							//remove info on positive input selection
		if (mode == NORMAL) {					//if external signal on negative input
			pinMode(A1, INPUT);					//ACMP0N is A1
			sbi(DIDR0, ADC5D);					//A1, diasable digital input
		}
		AC0CON = _BV(AC0EN) | mode;				//Set new input select, enable comparator

	} else if(pin == AC1) {
		if (mode < 7) {
			pinMode(A0, INPUT);					//ACMP1 is A0
			sbi(DIDR1, ADC10D);					//A0, diasable digital input
		} else {
			AMP1CSR |= _BV(AMPCMP1);			//connect output from analog amplifier 1
		}
		mode &= 0x07;							//remove info on positive input selection
		if (mode == NORMAL) {					//if external signal on negative input
			pinMode(2, INPUT);					//ACMP1N is D2
			sbi(DIDR0, ADC6D);					//D2, diasable digital input
		}
		AC1CON = _BV(AC1EN) | mode;				//Set new input select, enable comparator

	} else if(pin == AC2) {
		if (mode < 7) {
			pinMode(A2, INPUT);					//ACMP2 is A2
			sbi(DIDR0, ADC2D);					//A2, diasable digital input
		} else {
			AMP2CSR |= _BV(AMPCMP2);			//connect output from analog amplifier 2
		}
		mode &= 0x07;							//remove info on positive input selection
		if (mode == NORMAL) {					//if external signal on negative input
			pinMode(A3, INPUT);					//ACMP2N is A3
			sbi(DIDR0, ADC3D);					//A3, diasable digital input
		}
		AC2CON = _BV(AC2EN) | mode;				//Set new input select, enable comparator

	} else if(pin == AC3) {
		mode &= 0x07;							//no AMP output available
		pinMode(A4, INPUT);						//ACMP3 is A4
		sbi(DIDR1, ADC9D);						//A4, diasable digital input

		if (mode == NORMAL) {					//if external signal on negative input
			pinMode(A5, INPUT);					//ACMP1N is A5
			sbi(DIDR1, ADC8D);					//A5, diasable digital input
		}
		AC3CON = _BV(AC3EN) | mode;				//Set new input select, enable comparator

	// deactivation stuff
	} else {
		//amps
		if ((pin ==  4) || (pin ==  15)) {		// redefine AMP0 input
			AMP0CSR = 0;						// shutdown Op-Amp and release input ports.
			cbi(DIDR1, AMP0ND);					// AMP0-, enable digital input
			cbi(DIDR1, AMP0PD);					// AMP0+, enable digital input

		} else if ((pin == A4) || (pin == A5)) {// redefine AMP1 input
			AMP1CSR = 0;						// shutdown Op-Amp and release input ports.
			if ((~(AC3CON & 0x07))) {			// if pin not used by comp3
				cbi(DIDR1, ADC8D);				// AMP1-, enable digital input
			}
			if (AC3CON & _BV(AC3EN)) {			// if pin not used by comp3
				cbi(DIDR1, ADC9D);				// AMP1+, enable digital input
			}

		} else if ((pin == 10) || (pin == A6)) {// redefine AMP2 input
			AMP2CSR = 0;						// shutdown Op-Amp and release input ports.
			if ((~(AC1CON & 0x07))) {			// if pin not used by comp1
				cbi(DIDR0, ADC6D);				// AMP2-, enable digital input
			}
			cbi(DIDR1, AMP2PD);					// AMP2+, enable digital input
		}

		//comp
		if (((pin == 7) && !(AMP0CSR & _BV(AMPCMP0))) || ((pin == A1) && !(~(AC0CON & 0x07)))) {
			AMP0CSR &= ~_BV(AMPCMP0);
			AC0CON = 0;
			cbi(DIDR1, ACMP0D);					//D7, enable digital input
			cbi(DIDR0, ADC5D);					//A1, enable digital input

		} else if (((pin == A0) && !(AMP1CSR & _BV(AMPCMP1))) || ((pin == 2) && !(~(AC1CON & 0x07)))) {
			AMP1CSR &= ~_BV(AMPCMP1);
			AC1CON = 0;
			if (!(AMP2CSR & _BV(AMP2EN))) {		//if AMP2 is not in use
				cbi(DIDR0, ADC6D);				//D2, enable digital input
			}
			sbi(DIDR1, ADC10D);					//A0, enable digital input

		} else if (((pin == A2) && !(AMP2CSR & _BV(AMPCMP2))) || ((pin == A3) && !(~(AC2CON & 0x07)))) {
			AMP2CSR &= ~_BV(AMPCMP2);
			AC2CON = 0;
			cbi(DIDR0, ADC2D);					//A2, enable digital input
			cbi(DIDR0, ADC3D);					//A3, enable digital input

		} else if ((pin == A4) || ((pin == A5) && !(~(AC3CON & 0x07)))) {
			AC3CON = 0;
			if (!(AMP1CSR & _BV(AMP1EN))) {		//if AMP1 is not in use
				sbi(DIDR1, ADC9D);				//A4, enable digital input
				sbi(DIDR1, ADC8D);				//A5, enable digital input
			}
		}
	}

	if (pin == DAC_PORT) {
		DACON = 0x00;		// If DAC pin is being reassigned, make sure to turn off the DAC and release the pin.
	}
	// AT: Continue with original code.
#endif

	if (port == NOT_A_PIN) return;

	// JWS: can I let the optimizer do this?
	reg = portModeRegister(port);
	out = portOutputRegister(port);

	if (mode == INPUT) {
		uint8_t oldSREG = SREG;
                cli();
		*reg &= ~bit;
		*out &= ~bit;
		SREG = oldSREG;
	} else if (mode == INPUT_PULLUP) {
		uint8_t oldSREG = SREG;
                cli();
		*reg &= ~bit;
		*out |= bit;
		SREG = oldSREG;
	} else {
		uint8_t oldSREG = SREG;
                cli();
		*reg |= bit;
		SREG = oldSREG;
	}
}



// Forcing this inline keeps the callers from having to push their own stuff
// on the stack. It is a good performance win and only takes 1 more byte per
// user than calling. (It will take more bytes on the 168.)
//
// But shouldn't this be moved into pinMode? Seems silly to check and do on
// each digitalread or write.
//
// Mark Sproul:
// - Removed inline. Save 170 bytes on atmega1280
// - changed to a switch statment; added 32 bytes but much easier to read and maintain.
// - Added more #ifdefs, now compiles for atmega645
//
//static inline void turnOffPWM(uint8_t timer) __attribute__ ((always_inline));
//static inline void turnOffPWM(uint8_t timer)
static void turnOffPWM(uint8_t timer)
{
	switch (timer)
	{
		#if defined(TCCR1A) && defined(COM1A1)
		case TIMER1A:   cbi(TCCR1A, COM1A1);    break;
		#endif
		#if defined(TCCR1A) && defined(COM1B1)
		case TIMER1B:   cbi(TCCR1A, COM1B1);    break;
		#endif
		#if defined(TCCR1A) && defined(COM1C1)
		case TIMER1C:   cbi(TCCR1A, COM1C1);    break;
		#endif

		#if defined(TCCR2) && defined(COM21)
		case  TIMER2:   cbi(TCCR2, COM21);      break;
		#endif

		#if defined(TCCR0A) && defined(COM0A1)
		case  TIMER0A:  cbi(TCCR0A, COM0A1);    break;
		#endif

		#if defined(TCCR0A) && defined(COM0B1)
		case  TIMER0B:  cbi(TCCR0A, COM0B1);    break;
		#endif
		#if defined(TCCR2A) && defined(COM2A1)
		case  TIMER2A:  cbi(TCCR2A, COM2A1);    break;
		#endif
		#if defined(TCCR2A) && defined(COM2B1)
		case  TIMER2B:  cbi(TCCR2A, COM2B1);    break;
		#endif

		#if defined(TCCR3A) && defined(COM3A1)
		case  TIMER3A:  cbi(TCCR3A, COM3A1);    break;
		#endif
		#if defined(TCCR3A) && defined(COM3B1)
		case  TIMER3B:  cbi(TCCR3A, COM3B1);    break;
		#endif
		#if defined(TCCR3A) && defined(COM3C1)
		case  TIMER3C:  cbi(TCCR3A, COM3C1);    break;
		#endif

		#if defined(TCCR4A) && defined(COM4A1)
		case  TIMER4A:  cbi(TCCR4A, COM4A1);    break;
		#endif
		#if defined(TCCR4A) && defined(COM4B1)
		case  TIMER4B:  cbi(TCCR4A, COM4B1);    break;
		#endif
		#if defined(TCCR4A) && defined(COM4C1)
		case  TIMER4C:  cbi(TCCR4A, COM4C1);    break;
		#endif
		#if defined(TCCR4C) && defined(COM4D1)
		case TIMER4D:	cbi(TCCR4C, COM4D1);	break;
		#endif

		#if defined(TCCR5A)
		case  TIMER5A:  cbi(TCCR5A, COM5A1);    break;
		case  TIMER5B:  cbi(TCCR5A, COM5B1);    break;
		case  TIMER5C:  cbi(TCCR5A, COM5C1);    break;
		#endif
	}
}

void digitalWrite(uint8_t pin, uint8_t val)
{
	uint8_t timer = digitalPinToTimer(pin);
	uint8_t bit = digitalPinToBitMask(pin);
	uint8_t port = digitalPinToPort(pin);
	volatile uint8_t *out;

	if (port == NOT_A_PIN) return;

	// If the pin that support PWM output, we need to turn it off
	// before doing a digital write.
	if (timer != NOT_ON_TIMER) turnOffPWM(timer);

	out = portOutputRegister(port);

	uint8_t oldSREG = SREG;
	cli();

	if (val == LOW) {
		*out &= ~bit;
	} else {
		*out |= bit;
	}

	SREG = oldSREG;
}

int digitalRead(uint8_t pin)
{
	//read analog comparator output
	if (pin == AC0) {
		return (ACSR & _BV(AC0O)) ? HIGH : LOW;
	} else if (pin == AC1) {
		return (ACSR & _BV(AC1O)) ? HIGH : LOW;
	} else if (pin == AC2) {
		return (ACSR & _BV(AC2O)) ? HIGH : LOW;
	} else if (pin == AC3) {
		return (ACSR & _BV(AC3O)) ? HIGH : LOW;
	} else {
	// back to normal
		uint8_t timer = digitalPinToTimer(pin);
		uint8_t bit = digitalPinToBitMask(pin);
		uint8_t port = digitalPinToPort(pin);

		if (port == NOT_A_PIN) {
			return LOW;
		}

		// If the pin that support PWM output, we need to turn it off
		// before getting a digital reading.
		//FIXME: do the same for DAC and others?
		if (timer != NOT_ON_TIMER) {
			turnOffPWM(timer);
		}

		if (*portInputRegister(port) & bit) {
			return HIGH;
		}

		return LOW;
	}
}
