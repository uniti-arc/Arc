To setup the project and upload the Uniti ARC usbserial application firmware to an ATMEGA16U2 using the Uniti ARC USB DFU bootloader:
1. unpack the source into LUFA's (version 100807) Projects directory
2. set ARDUINO_MODEL_PID in the makefile as appropriate
3. do "make clean; make"
4. put the 16U2 into USB DFU mode:
4.a. assert and hold the 8U2's RESET line
4.b. release the 8U2's RESET line
5. confirm that the board enumerates as either "Uniti ARC DFU"
6. do "make dfu" (OS X or Linux - dfu-programmer must be installed first) or "make flip" (Windows - Flip must be installed first)

Check that the board enumerates as either "Uniti ARC".  Test by uploading a new Arduino sketch from the Arduino IDE.
