To setup the project and program an ATMEG16U2 with the Uniti ARC USB DFU bootloader:
1. unpack the source into LUFA's (version 100807) Bootloader directory
2. set ARDUINO_MODEL_PID in the makefile as appropriate
3. do "make clean; make; make program"

Check that the board enumerates as "Uniti ARC DFU".  Test by uploading the Uniti_ARC-usbserial application firmware (see instructions in Uniti_ARC-usbserial directory)

